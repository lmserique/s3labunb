#!/bin/bash

if [[ $# -lt 3 ]] ; then
    echo 'Parâmetros insuficientes!'
    echo 'Tente: deleteMysqlS3.sh <nome_da_base_de_dados> <nome_do_bucket> <data_do_backup>'
    exit 1
fi

# Realiza o parser do arquivo de credenciais do awscli
# Fornece aws_access_key_id
# Fornece aws_secret_access_key
source <(grep = ~/.aws/credentials | sed 's/ *= */=/g')

database=$1
bucket=$2
fileName=${database}_${3}.gz
destinationFile=backups/${fileName}
resource="/${bucket}/${destinationFile}"
contentType="text/plain"
data=`date -R`
stringToSign="DELETE\n\n${contentType}\n${data}\n${resource}"
signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${aws_secret_access_key} -binary | base64`

curl -X DELETE \
  -H "Host: ${bucket}.s3.amazonaws.com" \
  -H "Date: ${data}" \
  -H "Content-Type: ${contentType}" \
  -H "Authorization: AWS ${aws_access_key_id}:${signature}" \
  https://${bucket}.s3.amazonaws.com/${destinationFile}

