#!/bin/bash

if [[ $# -lt 2 ]] ; then
    echo 'Parâmetros insuficientes!'
    echo 'Tente: listMysqlS3.sh <nome_da_base_de_dados> <nome_do_bucket>'
    exit 1
fi

# Realiza o parser do arquivo de credenciais do awscli
# Fornece aws_access_key_id
# Fornece aws_secret_access_key
source <(grep = ~/.aws/credentials | sed 's/ *= */=/g')

database=$1
bucket=$2
resource="/${bucket}/"
contentType="text/plain"
data=`date -R`
stringToSign="GET\n\n${contentType}\n${data}\n${resource}"
signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${aws_secret_access_key} -binary | base64`
 
curl -s -X GET \
  -H "Host: ${bucket}.s3.amazonaws.com" \
  -H "Date: ${data}" \
  -H "Content-Type: ${contentType}" \
  -H "Authorization: AWS ${aws_access_key_id}:${signature}" \
  https://${bucket}.s3.amazonaws.com/?list-type=2\&prefix=backups/$database | xpath -q -e '/ListBucketResult/Contents/Key/text()'
