#!/bin/bash

if [[ $# -lt 2 ]] ; then
    echo 'Parâmetros insuficientes!'
    echo 'Tente: backupMysqlS3.sh <nome_da_base_de_dados> <nome_do_bucket>'
    exit 1
fi

# Realiza o parser do arquivo de credenciais do awscli
# Fornece aws_access_key_id
# Fornece aws_secret_access_key
source <(grep = ~/.aws/credentials | sed 's/ *= */=/g')

now="$(date +"%Y_%m_%d_%H_%M_%S")"
database=$1
bucket=$2
fileName=${database}_${now}.gz
destinationFile=backups/${fileName}
resource="/${bucket}/${destinationFile}"
contentType="application/gzip"
data=`date -R`
stringToSign="PUT\n\n${contentType}\n${data}\n${resource}"
signature=`echo -en ${stringToSign} | openssl sha1 -hmac ${aws_secret_access_key} -binary | base64`
sudo mysqldump --databases $database | gzip -c > /tmp/${fileName} 

curl -X PUT -T /tmp/${fileName} \
  -H "Host: ${bucket}.s3.amazonaws.com" \
  -H "Date: ${data}" \
  -H "Content-Type: ${contentType}" \
  -H "Authorization: AWS ${aws_access_key_id}:${signature}" \
  https://${bucket}.s3.amazonaws.com/${destinationFile}

rm -rf /tmp/${fileName}
